/* Mancuso, Logan
* Lab Exam 1 this program will take a user inputed number > 0 and will print some stairs
*/
import java.util.Scanner;

public class StairMaker {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
	Scanner keyboard = new Scanner(System.in);
	System.out.println("Enter the width and heigth and I will print some stairs");
	int stairs = keyboard.nextInt();
	if (stairs <= 0)
	{
		System.out.println("invalid input");
		System.exit(0);
	}
	int nextLine = (stairs);
	for (int i=0; i<=stairs-1; i++)//loop to control number of stairs
	{
		for (int j=0; j<nextLine; j++)//loop to build each step
		{
			for (int k=0; k<nextLine-1; k++)
			{
				System.out.print("*");
			}
			System.out.println("*");
		}
		nextLine= (nextLine+stairs);//make stair step below larger by value of input from user
	}
	System.out.println("Done !");
	}

}
