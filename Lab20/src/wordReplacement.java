import java.io.*;
import java.util.Scanner; 

public class wordReplacement {
	
	static final String IN_FILE_NAME = "lyrics.txt";
	static final String OUT_FILE_NAME = "newLyrics.txt";
	static final String DELIM = " ";

	public static void main(String[] args)
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter word to replace Danger with");
		String newWord = keyboard.nextLine(); //take user input of new word to replace with 
		wordReplacement.readFile(newWord);
		System.out.println("new lyrics printed to " + OUT_FILE_NAME);
	}

	public static void readFile (String newWord)
	{
		try
		{
			String result = "";
			Scanner fileScanner = new Scanner(new File(IN_FILE_NAME)).useDelimiter(" "); //create new spaces on each line
			while(fileScanner.hasNext())
			{
				String nextWord = fileScanner.next(); //as long as system recognizes a new token it will read file
				if (nextWord.toUpperCase().contains("DANGER"))
				{
					nextWord = newWord; //replace word with user input word
				}	
				result = result + nextWord + DELIM;
				PrintWriter fileWriter = new PrintWriter(new FileOutputStream(OUT_FILE_NAME));
				fileWriter.println(result+ DELIM);
				fileWriter.close(); //close and save file
			}
			
			fileScanner.close(); //close scanner 
		}
			catch (Exception e)
		{
			System.out.println(e.getMessage()); //if error occurs 
		}
	}
}

