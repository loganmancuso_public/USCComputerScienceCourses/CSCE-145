import java.util.Scanner;
import java.io.*;

public class VowelOrderReader {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("scanning the file: text.txt\n");
		String fileName = "text.txt";
		int count= 0; //counting the number of instances of the order "AEIOU"
		String vowelWord;
		String result = " ";
		try
		{
			Scanner fileScanner = new Scanner(new File(fileName));
			while (fileScanner.hasNext()) 
			{
				vowelWord = fileScanner.next().toLowerCase(); //reading the file 
				result = result.concat(vowelWord);  //storing the information into result
			}
			fileScanner.close();
			for (int i=0; i<result.length(); i++) //scanning using nested for loops
			{
				if (result.charAt(i)=='a')
				{
					for (int j=i+1; j<result.length();j++) //the next character to check order 
					{
						if (result.charAt(j)=='e')
						{
							for (int k=j+1; k<result.length(); k++)
							{
								if (result.charAt(k)=='i')
								{
									for (int l=k+1; l<result.length();l++)
									{
										if (result.charAt(l)=='o')
										{
											for (int m=l+1; m<result.length();m++)
											{
												if (result.charAt(m)=='u')
												{
													i=m;
													count ++; //iff it meets the criteria above then count goes up by one
													break;  //break for line 39 int m	
												}
											}
											break; //break for line 35 int l 
										}
									}
									break; //break for line 31 int k
								}
							}
							break; //break for line 27 int j
						}
					}
				}
			}
		} //end of try block 			
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("The file " +"\"" + fileName + "\"" + " contains  " + count + " instances of 'AEIOU' vowels in order"); //print results 
	}

}
