/****************************************************************
 * 'Aprog.java'
 * Main file for program
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 09-09-2018--12:39:24
 *
**/
import java.io.*;
import java.util.*;

public class Aprog {

  public static void main(String[] args) {
  final String in_file = args[0];
  final String out_file = args[1];
  final String log_file = args[2];
  String out_stream = "XX";
  String log_stream = "XX";

  out_stream = out_file + "\nBeginning Execution\n";
  log_stream = log_file + "\nBeginning Execution\n";

  /****************************************************************
   * Add Code Here:
  **/

		System.out.println("This program will evaluate the relationship between Volume, Mass and, Density of a given object");
		System.out.println("Volume= Mass/Density");
		System.out.println("What is the mass of the object and the density, plesase seperate with a space?");
		Scanner keyboard = new Scanner(System.in);
		int value1= keyboard.nextInt();
		int value2 = keyboard.nextInt();
		int result = value1/value2;
		System.out.println("The Volume of the object is " + result);
    System.out.println("program complete");
    keyboard.close();


  out_stream += "Ending Execution\n";
  log_stream += "Ending Execution\n";

  write_to_file(out_file, out_stream);
  write_to_file(log_file, log_stream);

  }//end main function

  /****************************************************************
   * Function 'write_to_file'
  **/
  private static void write_to_file(String file_name, String output) {
    try {
      PrintWriter file_writer = new PrintWriter(new FileOutputStream(file_name));
      file_writer.println(output); //print to file
      file_writer.close(); //close stream
    }//end try
    catch (Exception exception) {
      System.out.println(exception.getMessage()); //if error occurs
    }//end catch
  }//end write_to_file
}

/****************************************************************
 * End 'Aprog.java'
**/

