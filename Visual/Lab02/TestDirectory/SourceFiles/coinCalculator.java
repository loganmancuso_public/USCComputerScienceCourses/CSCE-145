import java.util.Scanner;
public class coinCalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter an amount from 1 to 99");
		Scanner keyboard = new Scanner(System.in);
		int initialvalue= keyboard.nextInt();
		//Quarters Start
		int remainderQuarters = initialvalue % 25;
		int newValueOfQuarters = initialvalue - remainderQuarters;
		int quantityOfQuarters = newValueOfQuarters / 25;
		int finalOfQuarters = initialvalue - (25*quantityOfQuarters);
		// Quarters done start new loop
		// Dimes Start
		int remainderOfDimes = finalOfQuarters % 10;
		int newValueOfDimes = finalOfQuarters - remainderOfDimes;
		int quantityOfDimes = newValueOfDimes / 10;
		int finalOfDimes = finalOfQuarters - (10*quantityOfDimes);
		// Dimes done start new loop
		// Nickels start
		int remainderOfNickles = finalOfDimes % 5;
		int newValueOfNickles = finalOfDimes - remainderOfNickles;
		int quantityOfNickles = newValueOfNickles / 5;
		int quantityOfPennies = finalOfDimes - (5*quantityOfNickles);
		// Nickels done start new loop
		System.out.println("Original value= " + initialvalue + 
				"\nQuarters= " + quantityOfQuarters + "\nDimes= " 
				+ quantityOfDimes + "\nNickels= " + quantityOfNickles 
				+ "\nPennies= " + quantityOfPennies); 
	}

}
