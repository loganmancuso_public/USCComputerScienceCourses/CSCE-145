import java.util.Scanner;
public class MoviePrgm {

	public static void main(String[] args) throws IndexException, FileFormatException, DirectorNotFoundException, YearException, RatingException{
		// TODO Auto-generated method stub

		MovieDatabase movieDB = new MovieDatabase();
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Welcome to the Movie Database");
		boolean quit = false;
		while(quit == false)
		{
			System.out.println("\nEnter 1 to Add a Movie\n"
					+ "Enter 2 to Remove a Movie\n"
					+ "Enter 3 to Sort movies (by title)\n"
					+ "Enter 4 to Sort movies (by rating)\n"
					+ "Enter 5 to Sort movies (by Box Office Gross)\n"
					+ "Enter 6 to Sort movies (by Director)\n"
					+ "Enter 7 to Display movies by a director\n"
					+ "Enter 8 to Print to a Database file\n"
					+ "Enter 9 to Read from a Database file\n"
					+ "Enter 0 to Quit");
			int selection = keyboard.nextInt();
			keyboard.nextLine();
			
			switch(selection)
			{
			case 1: //add movie
				System.out.println("Enter the name of the Movie");
				String name = keyboard.nextLine();
				System.out.println("Enter the year it was released");
				int year = keyboard.nextInt();
				System.out.println("Enter a rating (1 to 5)");
				int rating = keyboard.nextInt();
				System.out.println("Enter the Directors name (last,first)");
				String director = keyboard.nextLine();
				keyboard.nextLine();
				System.out.println("Enter the Box Office Gross");
				double boxOffGross = keyboard.nextDouble();
				movieDB.addMovie(new Movie(name,year,rating,director,boxOffGross));			
				break;
				
			case 2: //remove movie
				System.out.println("Enter the name of the movie you want removed");
				String aName = keyboard.nextLine();
				movieDB.removeMovie(aName);
				break;
				
			case 3: //sort by title
				System.out.println("Sorting by Title");
				movieDB.sortByTitle();
				break;
				
			case 4: //sort by rating 
				System.out.println("Sorting by rating");
				movieDB.sortByTitle(); //sort by title first because some movies may have same rating but having in alpha order makes it neater
				movieDB.sortByRating();
				break;
				
			case 5: //sort by BOG
				System.out.println("Sorting by Box Office Gross");
				movieDB.sortByTitle(); //if odd case of same boxOffGross its already sorted in alpha order makes it look neater
				movieDB.sortByBOG();
				break;
				
			case 6: //sort by Director
				System.out.println("Sorting by Director ");
				movieDB.sortByDirector();
				break;				
				
			case 7: //display movies by director
				/*
				 * this was a mistake by me I read the directions wrong but it works fine and I don't like to delete code 
				 * the proper code is above in case 6 sorting by the director not displaying movies by a director
				 * as in this case#7 where a user inputs a directors name and it prints to the console the info 
				 * associated with that directors name  
				 */
				System.out.println("Enter name of the Director");
				String theDirector = keyboard.nextLine();
				movieDB.showMoviesByDirector(theDirector);
				break;
				
			case 8: //print DB file
				System.out.println("Enter name of file to write to");
				String aFileName = keyboard.nextLine();
				movieDB.writeMovieDBFile(aFileName);
				break;
				
			case 9: //read DB file
				System.out.println("Enter name of file to read");
				String fileName = keyboard.nextLine();
				movieDB.readMovieDBFile(fileName);
				break;
				
			case 0: //quit
				quit=true;
				break;
				
			default: //other
				System.out.println("Invalid choice");
				break;
			}
		}
		System.out.println("Good Bye");
		keyboard.close();
	}

}



