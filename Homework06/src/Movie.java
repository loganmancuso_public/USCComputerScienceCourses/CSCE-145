
public class Movie {

	//instance variables
	private String name;
	private int year;
	private int rating;
	private String director;
	private double boxOffGross; //box office gross
	
	//default
	public Movie()
	{
		this.name = "None";
		this.year = 0;
		this.rating = 0;
		this.director = "None";
		this.boxOffGross = 0.0;
	}
	//parameterized 
	public Movie(String aName, int aYear, int aRating, String aDirector, double aBoxOffGross) throws YearException, RatingException
	{
		this.name = aName;
		this.setYear(aYear); //use mutator  
		this.setRating(aRating);//use mutator
		this.director = aDirector;
		this.boxOffGross = aBoxOffGross;
	}
	//accessors
	public String getName()
	{
		return this.name;
	}
	public int getYear()
	{
		return this.year;
	}
	public int getRating()
	{
		return this.rating;
	}
	public String getDirector()
	{
		return this.director;
	}
	public double getBoxOffGross()
	{
		return this.boxOffGross;
	}
	//mutator
	public void setName(String aName)
	{
		this.name = aName;
	}
	public void setYear(int aYear) throws YearException
	{
		if (aYear<=2015)
		{
			this.year = aYear;
		}
		else 
		{
			//throw year out of bounds exception
			System.out.println("Invalid year");
			throw new YearException();
			
		}
	}
	public void setRating(int aRating) throws RatingException
	{
		if (aRating <=5 || aRating >=1)
		{
			this.rating = aRating;
		}
		else
		{
			//throw rating out of bounds exception
			throw new RatingException();
		}
	}
	public void setDirector(String aDirector)
	{
		this.director = aDirector;
	}
	public void setBoxOffGross(double aBoxOffGross)
	{
		this.boxOffGross = aBoxOffGross;
	}
}
