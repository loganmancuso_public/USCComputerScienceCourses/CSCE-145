/*Mancuso, Logan
 * This program will sort an array of numbers using insertion sort method
 */
import java.util.Scanner;

public class insertionSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the quantity of numbers you wish to sort");
		int quantity = keyboard.nextInt();
		keyboard.nextLine(); //prevent error
		int arrayOrigNum[] = new int[quantity];
		int arraySortNum[] = new int[quantity];
		//populate array
		for (int i=0; i<arrayOrigNum.length; i++)
		{
			System.out.print("Enter a number");
			int number = keyboard.nextInt();
			keyboard.nextLine();
			arrayOrigNum[i] = number;
		}
		//duplicate array
		for (int i=0; i<quantity; i++)
		{
			arraySortNum[i] = arrayOrigNum[i];
		}
		//sort the array
		int j, temp, i;
		for (j=1; j<quantity; j++)
		{
			temp = arraySortNum[j];
			for (i=j-1; (i>=0)&&(temp<arraySortNum[i]); i--) 
			{
				arraySortNum[i+1] = arraySortNum[i];
			}
			arraySortNum[i+1] = temp;
		}
		//print arrays
		System.out.println("\nThe original array is:");
		for (i=0; i<quantity; i++)
		{
			System.out.print(arrayOrigNum[i]+" ");
		}
		System.out.println("\n\nThe sorted array is:");
		for (i=0; i<quantity; i++)
		{
			System.out.print(arraySortNum[i]+" ");
		}
		System.out.println("\n\nProgram Complete");
		System.exit(0);
	}

}
