
public class DailySchedule {
	
	public static final int DAILY_PLANNER_SIZE = 24;

	private Activity[] activities;
	
	public DailySchedule()
	{
		this.activities = null;
	}
	public Activity[] setActivities()
	{
		return this.activities;
	}
	public void getActivities(Activity[] activities)
	{
		this.activities=activities;
	}
	
	
	//bubble sort by start time
	public void sortByTime() 
	{
		boolean swapped = true;
		while(swapped == true)
		{
			swapped = false;
			for(int i=0;i<DAILY_PLANNER_SIZE-1;i++)
			{
				if(activities[i+1]==null)
				{
					break;
				}
				if(activities[i].getStartHour() >  activities[i+1].getStartHour())//Out of order
				{
					Activity temp = activities[i];
					activities[i] = activities[i+1];
					activities[i+1] = temp;
					swapped = true;
				}
			}
		}
	}
	
	//add activity
	public void addActivity(Activity anActivity)
	{
		for (int i=0; i<DAILY_PLANNER_SIZE ;i++)
		{
			if (activities[i]==null)
			{
			activities[0] = anActivity;
			break;
			}
		}
	}
	
	//remove activity
	public void removeActivity(String aName) 
	{
		int removeIndex = -1; //invalid index use for when activity not found
		for (int i=0; i<DAILY_PLANNER_SIZE;i++)
			{
				if (activities[i].getName().equals(aName))//if the name matches the one inputed 
				{
					removeIndex = i; //set index to one that will be removed 
					break;
				}
			}
		if(removeIndex == -1)
			{
				System.out.println("Activity was not found");
			}
		else
		{	//push all other values up one value to overwrite
			for (int i=removeIndex;i<activities.length-1;i++)
			{
				activities[i] = activities[i+1];
			}
			activities[activities.length-1] = null; //last values set to null when removed and shifted or you will have a duplicate of last value 
			System.out.println("Activity was removed");	
		}		
	}
	
	public void printActivities()
	{
		for (int i=0; i<activities.length;i++)
		{
			if(activities[i]==null)
				{
					break;
				}
			else 
			{
				activities[i].toAString(null);
				//System.out.println("Name: "+activities[i].getName()+" Start Time: "+activities[i].getStartHour()+" End Hour: "+ activities[i].getEndHour());
			}
		}
	}
}

