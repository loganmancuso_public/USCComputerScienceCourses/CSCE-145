import java.io.*;
import java.util.*;


public class RPGPlayer
{
	//instance variables 
	private String Name;
	private int HP;
	private int strength;
	private double speed;
	private String weapon;
	public static final String delim = " ";
 
	//default
	public RPGPlayer() 
	{
		Name ="";
		HP = 0;
		strength = 0;
		speed = 0.0;
		weapon ="";
	}
 
	//parameterized 
	public RPGPlayer(String aName, String aWeapon, int AnHP, int aStrength, double aSpeed) 
	{
		Name = aName;
		weapon = aWeapon;
		strength = aStrength;
		HP = AnHP;
		speed = aSpeed;
	}
	//reading file
	public RPGPlayer(String file)
	{
		readPlayerFile(file);
	}
	//parameterized
	public RPGPlayer(String string, int i, int j, double d, String string2) 
	{
		Name =string;
		HP = i;
		strength = j;
		speed = d;
		weapon =string2;
	}

	//accessors
	public String getName()
	{
		return this.Name;
	}
	public String getWeapon()
	{
		return this.weapon;
	}
	public int getHP()
	{
		return this.HP;
	}
	public int getStrength()
	{
		return this.strength;
	}
	public double getSpeed()
	{
		return this.speed;
	}
	
	//mutators
	public void setName(String aName)
	{
		this.Name = aName;
	}
	public void setWeapon(String aWeapon)
	{
		this.weapon = aWeapon;
	}
	public void setHP(int AnHP)
	{
		this.HP = AnHP;
	}
	public void setStrength(int aStrength)
	{
		if(aStrength > 0)
		{
			this.strength = aStrength;
		}
		else
		{
			System.out.println("Negative speed is forbidden");
		}
	}
	public void setSpeed(double aSpeed)
	{
		if(aSpeed > 0)
		{
			this.speed = aSpeed;
		}
		else
		{
			System.out.println("Negative speed is forbidden");
		}
	}
	
	//other methods 
	
	//wrtiting the player file 
	public void writePlayerFile(String write)
	{
		try
		{
			PrintWriter fileWrite = new PrintWriter(new FileOutputStream(write),true);
			fileWrite.println("Name " + Name);
			fileWrite.println("Speed " + speed);
			fileWrite.println("Strength " + strength);
			fileWrite.println("Weapon " + weapon);
			fileWrite.println("HP "+ HP);
			 
			fileWrite.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	//reading the player file
	public void readPlayerFile(String afile)
	{
		try
		{
			Scanner kb = new Scanner(new File(afile));
			while(kb.hasNext())
			{
				String line = kb.nextLine();
				String[] splitlines = line.split(delim);
				String Name = splitlines[0];
				if(splitlines.length < 2)
				{
					System.out.println("ERROR");
					throw new IOException();
				}
				if(Name.equals("Name"))
				{
					String Name_value = "";
					for(int i = 1; i < splitlines.length; i++)
					{
						Name_value += " " + splitlines[i];
					}
					this.setName(Name_value);
				}
				else if (Name.equals("HP"))
				{
					String hp_text_value = splitlines[1];
					try
					{
						int hp_value = Integer.parseInt(hp_text_value);
						this.setHP(hp_value);
					}
					catch(NumberFormatException e)
					{
						System.out.println(e.getMessage());
					}
				}
				else if (Name.equals("Strength")) 
				{
					String strength_text_value = splitlines[1];
					try
					{
						int strength_value = Integer.parseInt(strength_text_value);
						this.setStrength(strength_value);
					} 
					catch(NumberFormatException e)
					{
						System.out.println(e.getMessage());
					}
				}
				else if (Name.equals("Speed"))
				{
					String spd_value=splitlines[1];
					double speed_value = Double.parseDouble(spd_value);
					this.setSpeed(speed_value);
				}
				else if (Name.equals("Weapon"))
				{
					String weapon_value = "";
					for(int i = 1; i < splitlines.length; i++)
					{
						weapon_value += " " + splitlines[i];
					}
					this.setWeapon(weapon_value);
				}
			}
			kb.close();
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage());
		}
	}
	//printing the info to console 
	public void printInfo()
	{
		System.out.println("The player name " +Name);
		System.out.println("HP: " +HP);
		System.out.println("Strength: " +strength);
		System.out.println("Speed: " +speed);
		System.out.println("Weapon: " +weapon);
	}
} 
