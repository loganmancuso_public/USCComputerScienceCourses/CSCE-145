// Mancuso, Logan 
import java.util.Scanner;
public class grader {

	//attributes, create the instance variables 
	private double quizOne;
	private double quizTwo;
	private double midtermExam;
	private double finalExam;
	private double finalGrade;
	
	//Accessors, make public to user to input data 
	Scanner keyboard = new Scanner(System.in);//take user input
	public void setquizOne()
	{
		System.out.println("Enter grade for quiz one");
		double aquizOne = keyboard.nextDouble();
		if (aquizOne > 100 || aquizOne < 0) //find invalid inputs 
		{
			System.out.println("Invalid grade, (out of bounds 0->100) system exit \n");
			System.exit(0); //exit if an input is out of bounds 
		}
		this.quizOne = aquizOne;
	}
	public void setquizTwo()
	{
		System.out.println("Enter grade for quiz two");
		double aquizTwo = keyboard.nextDouble();
		if (aquizTwo > 100 || aquizTwo < 0)//find invalid inputs 
		{
			System.out.println("Invalid grade, (out of bounds 0->100)system exit \n");
			System.exit(0);//exit if an input is out of bounds 
		}
		this.quizTwo = aquizTwo;
	}
	public void setmidtermExam()
	{
		System.out.println("Enter grade for the midterm");
		double amidtermExam = keyboard.nextDouble();
		if (amidtermExam > 100 || amidtermExam < 0)//find invalid inputs 
		{
			System.out.println("Invalid grade, (out of bounds 0->100) system exit \n");
			System.exit(0);//exit if an input is out of bounds 
		}
		this.midtermExam = amidtermExam;
	}
	public void setfinalExam()
	{
		System.out.println("Enter grade for the final");
		double afinalExam = keyboard.nextDouble();
		if (afinalExam > 100 || afinalExam < 0)//find invalid inputs 
		{
			System.out.println("Invalid grade, (out of bounds 0->100) system exit \n");
			System.exit(0);//exit if an input is out of bounds 
		}
		this.finalExam = afinalExam; 
	}
	public void calculateFinalGrade()
	{
		double quizPercent = (quizOne*12.5)+(quizTwo*12.5);//each quiz worth 12.5%
		double midtermExamPercent = (midtermExam*25); //midterm worth 25%
		double finalExamPercent = (finalExam*50);//final worth 50%
		double calculateFinalGrade = (quizPercent+midtermExamPercent+finalExamPercent)/100; //divide by 100 for final grade
		this.finalGrade = calculateFinalGrade;//store grade for use in display function
	}
	
	//method 
	public void printFinalGrade ()
	{
		//convert to letter grade and print both 
		if (finalGrade >= 90 && finalGrade <= 100)//bounds of an A 90-100
		{
			System.out.println("The final grade is: \n"+finalGrade+ " you got an A");
		}
		else if (finalGrade >= 80 && finalGrade <= 89)//bounds of a B 80-89
		{
			System.out.println("The final grade is: \n"+finalGrade+ " you got an B");
		}
		else if (finalGrade >= 70 && finalGrade <= 79)//bounds of a C 70-79
		{
			System.out.println("The final grade is: \n"+finalGrade+ " you got an C");
		}
		else if (finalGrade >= 60 && finalGrade <= 69)//bounds of a D 60-69
		{
			System.out.println("The final grade is: \n"+finalGrade+ " you got an D");
		}
		else if (finalGrade >= 0 && finalGrade < 60)//bounds of a F 0-60
		{
			System.out.println("The final grade is: \n"+finalGrade+ " you got an F");
		}
		else //if user calculation in line 97 doesn't calculate final grade right and cannot determine letter association  
		{
			System.out.println("error in math recheck scores, (final less than zero || greater than 100)");//make sure math in line 97 is correct
		}
	}
			
}//end of public class grader {
