
public class Square extends SquareDriver{

	//instance variables 
	private int length;

	//default constructor
	public Square()
	{
		this.length = 1;
	}
	
	//Parameterized constructor 
	public Square (int aLength)
	{
		aLength = length;
	}
	
	//accessor
	public int getLength()
	{
		return this.length;
	}
	
	//mutator 
	public void setLength(int newLength)
	throws DimensionException
	{
		if (newLength <= 0) 
		{	
			throw new DimensionException();	//call to error checking: DimensionsException class
		}
		else //when dimensions are less than 0 
		{
			this.length=newLength;
		}
	}
	
	//otherMethods
	public void draw() //draw square
	{
		for (int i=0; i<length; i++) //print * quantity of length across
		{
			for (int j=0; j<length; j++)
			{
				System.out.print('*');
			}
		System.out.println(); //skipping length number of lines
		}
	}
	
	public int getArea()//evaluating the area
	{
		int Area = 0; //multiply dimensions for area
		Area= length*length; 
		return Area;	
	}
	
	public int getPerimeter()//evaluating the perimeter
	{
		int Perimeter = 0;
		Perimeter = (length*4)-4; //minus 4 if counting number of stars and multiply dimensions by 4 for perimeter
		return Perimeter;
	}
}
