import java.util.Scanner;
public class MatrixDatabase {

	//operations:
	
	//add matrix
	public void addMatrix(int heightOne, int heightTwo, int widthOne, int widthTwo, double[][] matrixOne, double [][] matrixTwo)
	{
		if (heightOne != heightTwo || widthOne != widthTwo)
		{
			System.out.println("Dimension mismatch");
		}
		else 
		{
			int height = (heightOne+heightTwo)/2;
			int width = (widthOne+widthTwo)/2;						
			double[][] result = new double [height][width];
			for (int i=0; i<height; i++)
			{
				for (int j=0; j<width; j++)
				{
					result[i][j] = matrixOne[i][j]+matrixTwo[i][j]; //operation 
					System.out.print(result[i][j]+"\t");
				}
				System.out.println("\n");
			}
			System.out.println("\n");
		}
	}
	//subtract
	public void subMatrix(int heightOne, int heightTwo, int widthOne, int widthTwo, double[][] matrixOne, double [][] matrixTwo)
	{
		if (heightOne != heightTwo || widthOne != widthTwo)
		{
			System.out.println("Dimension mismatch");
		}
		else 
		{
			int height = (heightOne+heightTwo)/2;
			int width = (widthOne+widthTwo)/2;						
			double[][] result = new double [height][width];
			for (int i=0; i<height; i++)
			{
				for (int j=0; j<width; j++)
				{
					result[i][j] = matrixOne[i][j]-matrixTwo[i][j];
					System.out.print(result[i][j]+"\t");
				}
				System.out.println("\n");
			}
			System.out.println("\n");
		}
	}
	//divide
		public void divMatrix(int heightOne, int heightTwo, int widthOne, int widthTwo, double[][] matrixOne, double [][] matrixTwo)
		{
			if (heightOne != heightTwo || widthOne != widthTwo)
			{
				System.out.println("Dimension mismatch");
			}
			else 
			{
				int height = (heightOne+heightTwo)/2;
				int width = (widthOne+widthTwo)/2;						
				double[][] result = new double [height][width];
				for (int i=0; i<height; i++)
				{
					for (int j=0; j<width; j++)
					{
						result[i][j] = matrixOne[i][j]/matrixTwo[i][j];
						System.out.print(result[i][j]+"\t");
					}
					System.out.println("\n");
				}
				System.out.println("\n");
			}
		}
	
	//multiply
	public void multMatrix(int heightOne, int heightTwo, int widthOne, int widthTwo, double[][] matrixOne, double [][] matrixTwo)
	{
		if (widthOne != heightTwo)
		{
			System.out.println("Dimension mismatch");
		}		
		else 
		{
			int height = heightOne;
			int width = widthTwo;
			double [][]result = new double [height][width]; //find dimensions of final matrix
			for (int i=0; i<heightOne; i++) 
			{
				for (int j=0; j<widthTwo; j++)
				{
					for (int k=0; k<widthOne; k++) 
					{
						result[i][j] += matrixOne[i][k]*matrixTwo[k][j]; //perform multiplication 
					}
					System.out.print(result[i][j]+"\t"); //print the resulting matrix
				} 
				System.out.println();
			}
		}
		System.out.println("\n");
	}
}
