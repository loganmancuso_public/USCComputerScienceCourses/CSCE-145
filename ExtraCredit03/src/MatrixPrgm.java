import java.util.Scanner;
public class MatrixPrgm {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MatrixDatabase matrixDB = new MatrixDatabase();
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Welcome to the Matrix Machine");
		boolean quit = false;
		while(quit == false)
		{
			System.out.println("Press 1 to continue or 9 to quit");
			String temp = keyboard.next();
			if(temp.contains("9"))
			{
				quit = true;
				break;
			}
			//first matrix
			System.out.println("Enter the Height and Width of the matrix (seperated by a space)");
			int heightOne = keyboard.nextInt(); //height for matrix one
			int widthOne = keyboard.nextInt(); //width for matrix one
			double [][] matrixOne = new double [heightOne][widthOne];
			for (int j=0; j<heightOne; j++)
			{
				for (int k=0; k<widthOne; k++)
				{
					System.out.println("Enter the data for the first matrix at ("+ (j+1) + "," + (k+1)+")");
					double data = keyboard.nextDouble();// input number
					matrixOne[j][k] = data;
				}
			}
			//second matrix
			System.out.println("Enter the Height and Width of the matrix (seperated by a space)");
			int heightTwo = keyboard.nextInt(); //height for matrix one
			int widthTwo = keyboard.nextInt(); //width for matrix one
			double [][] matrixTwo = new double [heightTwo][widthTwo];
			for (int j=0; j<heightTwo; j++)
			{
				for (int k=0; k<widthTwo; k++)
				{
					System.out.println("Enter the data for the first matrix at ("+ (j+1) + "," + (k+1)+")");
					double data = keyboard.nextDouble();// input number
					matrixTwo[j][k] = data;
				}
			}
			//what operation you want to perform on the two matrices
			System.out.println("Enter 1 to add, 2 to subtract, 3 to multiply, or 4 to divide");
			int choice = keyboard.nextInt();
			
			switch(choice) //switch on the type of operation and call to the method to print the resulting matrix from the operation 
			{
				case 1:
					matrixDB.addMatrix(heightOne, heightTwo, widthOne, widthTwo, matrixOne, matrixTwo);
					break;
				case 2:
					matrixDB.subMatrix(heightOne, heightTwo, widthOne, widthTwo, matrixOne, matrixTwo);
					break;
				case 3:
				matrixDB.multMatrix(heightOne, heightTwo, widthOne, widthTwo, matrixOne, matrixTwo);
				
					break;
				case 4:
					matrixDB.divMatrix(heightOne, heightTwo, widthOne, widthTwo, matrixOne, matrixTwo);
					break;
				default:
					System.out.println("Invalid coice");
					break;
			}
		}
	}
}
