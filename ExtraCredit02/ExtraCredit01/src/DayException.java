
public class DayException extends Exception{
	
	public DayException()
	{
		super("Incorrect Date"); 
		//if day not formatted correctly or out of bounds for that month (1-31)
	}
	public DayException(String aMsg)
	{
		super (aMsg);
	}

}
