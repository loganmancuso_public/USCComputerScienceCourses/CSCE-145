import java.util.Scanner;

public class DateConverter {
	
	public static void main(String[] args) throws MonthException, DayException {
		// TODO Auto-generated method stub

		DateConverterBackend monthChecking = new DateConverterBackend();
		System.out.println("\nWelcome to Date Exceptions!");
		boolean quit = false;
		while(quit == false)
		{
			System.out.println("\nEnter 1 to contiunue or 9 to quit");
			Scanner keyboard = new Scanner(System.in);
			int choice = keyboard.nextInt();
			switch (choice)
			{
			case 1:
				System.out.println("Enter a date formatted: mm/dd");
				keyboard.nextLine();
				String date = keyboard.nextLine();//take in the date
				monthChecking.MonthError(date);
				String[] splitDate = date.split("/"); //split at / mark
				String theMonth = splitDate[0]; 
				String theDay = splitDate[1];
				int month = Integer.parseInt(theMonth);
				int day = Integer.parseInt(theDay);
				monthChecking.monthChecking(month, day);
				break;
			case 9:
				quit=true;
				break;
			} //switch on choice 
		}	//while loop
	}

}
